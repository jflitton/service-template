# A full stack Angular 4 app using snapi and TypeScript

## Intro
The goal of this project is to provide a succinct example of a full-stack Angular 4 application using TypeScript.  The server side uses [snapi](https://gitlab.com/jflitton/snapi), which mates Restify and Swagger 2.0 together.  The client side uses the Angular CLI.

## Developing
`npm run dev`

This will do the following:
1. Launch `ng serve` on port 4200 for serving up the Angular app and re-loading it when the client typescript files change
2. Launch the backend server on port 8081, re-launching it when the server typescript files change
3. Proxy all calls from the angular server to the backend server
4. Open a tab in your default browser to the root of your Angular app

## Testing
`npm test`

This will do the following:
1. Lint the client and server typescript files
2. Run the client unit tests

## Building
`npm run build`

Will build the app for production use.  The output will be in ./dist

## Launching in production
`npm start <port number>`

## To Do
1. Add server side tests
2. Add code coverage
3. Add gitlab CI pipeline
