import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

@Injectable()
export class JsonService {
  private headers = new Headers({ 'Content-Type': 'application/json', 'charset': 'UTF-8' });
  private options = new RequestOptions({ headers: this.headers });

  constructor(private http: Http) {}

  get(url: string): Observable<any> {
    return this.http.get(url).map(res => res.json());
  }

  patch(url: string, body: Object): Observable<any> {
    return this.http.patch(url, JSON.stringify(body), this.options);
  }

  post(url: string, body: Object): Observable<any> {
    return this.http.post(url, JSON.stringify(body), this.options);
  }

  put(url: string, body: Object): Observable<any> {
    return this.http.put(url, JSON.stringify(body), this.options);
  }
}
