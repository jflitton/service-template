import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import {HttpClient} from "@angular/common/http";

export class PingResult extends Object {
  success: boolean;
}

@Injectable()
export class PingService {
  constructor(private httpClient: HttpClient) {}

  getPing(): Observable<PingResult> {
    return this.httpClient.get<PingResult>('/v1.0/ping')
  }
}
