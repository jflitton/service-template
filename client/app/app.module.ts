import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core'
import { HttpClientModule } from '@angular/common/http'

import { RoutingModule } from './routing.module'
import { SharedModule } from './shared/shared.module'
import { AppComponent } from './app.component'
import { AboutComponent } from './about/about.component'
import { NotFoundComponent } from './not-found/not-found.component'
import { JsonService } from './services/json.service'
import { PingService } from './services/ping.service'


@NgModule({
  declarations: [
    AppComponent,
    AboutComponent,
    NotFoundComponent
  ],
  imports: [
    RoutingModule,
    SharedModule,
    HttpClientModule
  ],
  providers: [
    JsonService,
    PingService
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  bootstrap: [AppComponent]
})

export class AppModule { }
