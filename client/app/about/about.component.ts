import { Component } from '@angular/core'
import { PingResult, PingService } from '../services/ping.service'

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss']
})
export class AboutComponent {
  pingStatus = 'pending';

  constructor(pingService: PingService) {
    pingService.getPing()
      .subscribe((data: PingResult) => this.pingStatus = data.success === true ? 'success' : 'failure');
  }
}
