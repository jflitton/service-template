process.env.NODE_ENV = 'live';
import * as server from './server';

const port = process.argv.length > 2 ? process.argv[2] : 8081;
server.start(port);

const shutdown = () => process.exit(0);

process.on('SIGTERM', shutdown);
process.on('SIGINT', shutdown);
