import api from './api'
import * as snapi from 'snapi2'
import * as handlers from './handlers'
import * as auth from './middleware/auth'
import * as secureRedirect from './middleware/secureRedirect'
import config from './config'

export function start(port) {
  snapi.createServer({
    api,
    log: config.log,
    serveStatic:
      [
        {
          url: "/",   // assume anything on / is a static asset
          directory: `${__dirname}/../public`,
          default: 'index.html',
          maxAge: 0
        },
        {
          url: /\//,  // turn off caching for root so we don't get infinite loops with stormpath
          directory: `${__dirname}/../public`
        }
      ],
    routeHandlers: handlers,
    middleware: {
      afterParsers: [],
      beforeRoutes: [secureRedirect.httpsRedirect, auth.authenticate]
    }
  })
  .then(function (server) {
    // Start the server
    server.listen(port);
    return console.log(`API server started at ${server.url}.`);
  });
}
