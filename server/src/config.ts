export default {
  redirectHttps: false,
  log: {
    path: "/var/log/service-template/service-template.log",
    type: "rotating-file",
    level: "debug",
    period: "1d",
    count: 10
  }
};
