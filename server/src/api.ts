export default {
  swagger: '2.0',
  info: {
    version: '1.0',
    title: 'Service Template',
    description: 'For service stuff'
  },
  host: 'service-template',
  schemes: [ 'https' ],
  consumes: [ 'application/json' ],
  produces: [ 'application/json' ],
  security: [],
  paths: {
    '/v1.0/ping': {
      get: {
        security: [],
        description: 'Returns an indication of service availability',
        operationId: 'ping',
        responses: {
          200: {
            description: 'an indication of service availability',
            schema: {
              type: 'object',
              properties: { status: {
                type: 'string',
                enum: [ 'success' ]
              }
            }
            }
          }
        }
      }
    },
    '/v1.0/docs': {
      get: {
        description: 'Returns the API documentation',
        operationId: 'getDocs',
        responses: {
          '200': {
            description: 'API Documentation',
            schema: { 'type': 'object' }
          }
        }
      }
    }
  }
}
