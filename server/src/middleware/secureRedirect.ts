import config from '../config'
const AwsHeader = 'X-Forwarded-Proto'

export function httpsRedirect(req, res, next) {
  // /ping path *must* not be available from http, or instance won't start
  if ((req.path() === '/ping')  || !config.redirectHttps || ('https' === req.header(AwsHeader))) { return next(); }

  return res.redirect(`https://${req.header('Host')}${req.url}`, next);
}
